module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  },
  devServer: {
    proxy: {
      '^/assets': {
        target: `http://${process.env.HOST || 'localhost'}:${process.env.PORT || 8080}`,
        changeOrigin: true
      },
      '^/(?!ws)': {
        target: 'http://localhost:8081'
      }
    }
  }
}