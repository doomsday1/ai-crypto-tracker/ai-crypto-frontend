import { createApp } from 'vue'
import { createWebHistory, createRouter } from "vue-router"
import App from './App.vue'

import VueResizeText from 'vue3-resize-text';

const routes = [];

const router = createRouter({
    history: createWebHistory(),
    routes
});


const app = createApp(App);
app.use(router);
app.directive('ResizeText', VueResizeText.ResizeText);
app.mount('#app');